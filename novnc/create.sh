#! /usr/bin/env sh

if [ ! -f "$1/Dockerfile" ]; then
  echo "Dockerfile missing..."
  exit 0
else
  echo "Use path $1"
  cd $1
fi

# Set sh flags
set -euxo pipefail

# Default branch leaves tag empty (= latest tag)
# All other branches are tagged with the escaped branch name (commit ref slug)
if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
  tag=""
  echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
else
  tag=":$CI_COMMIT_REF_SLUG"
  echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
fi

# Build image
echo "Base Image alpine version 3.16"             #default= "3.16"
echo "CI_REGISTRY_IMAGE \"${CI_REGISTRY_IMAGE}\"" #default= "registry.gitlab.com/awesomemaker/docker-novnc
echo "Tag \"${tag}\""                             #trunk= ""; else e.g. merge-request <USER>-patch-<lfid>
DOCKER_BUILDKIT=1 docker build -f Dockerfile --build-arg "VersImage=slim" -t "$CI_REGISTRY_IMAGE${tag}" .

# Push gitlab project to regsitry, if main
if [[ -z "$tag" ]]; then
  echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  docker push "$CI_REGISTRY_IMAGE${tag}"
  docker logout $CI_REGISTRY
fi
